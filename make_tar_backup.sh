#!/bin/bash

d=$(date +%Y-%m-%d_%H%M%S)

filename="/backups/backup_$d.tar.gz"

echo ""
echo "Tar backup started --- $d"
echo "Destination: $filename"
echo "Backing up as: $(whoami)"

cd /

tar -cpzf $filename \
--exclude=backups \
--exclude=dev \
--exclude=proc \
--exclude=run \
--exclude=sys \
--exclude=tmp \
--exclude=media \
--exclude=mnt \
--exclude=lost+found \
./

# Check tar result
if [ $? == 0 ]; then
        echo "Backup successful!"
else
        echo "Something went wrong..."
fi

d=$(date +%Y-%m-%d_%T)

echo "Stopped at $d"
