# Tar Backups

Simply run `./make_tar_backup.sh` to make a backup.  
Edit the file first to modify the excludes and set a target destination!

## Options

The tar options are:

 * `-c` Create archive
 * `-p` Keep permissions and owners
 * `-z` Use compression
 * `-f` Put result in a file

Note that tar backups should always be made from relative paths! So we first `cd` to `/` and then archive the current directory.
